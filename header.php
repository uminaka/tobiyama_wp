<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tobiyama
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="body">
<div id="spFlg"></div>
<!-- <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'tobiyama' ); ?></a> -->

<header class="l-header header" id="header">
	<div class="l-header__inner">
		<?php
		if ( is_front_page() && is_home() ) : ?>
			<h1 class="header-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-logo__link" rel="home">
					<img src="/images/logo_header_pc.png" class="js-responsive-image" alt="映像便利屋 飛山企画">
				</a>
			</h1>
		<?php else : ?>
			<p class="header-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header-logo__link" rel="home">
					<img src="/images/logo_header_pc.png" class="js-responsive-image" alt="映像便利屋 飛山企画">
				</a>
			</p>
		<?php
		endif; ?>

		<a href="#" class="toggle-menu show-sp" id="js-toggle-menu">
			<span class="toggle-menu__btn" id="js-toggle-btn"></span>
			<span class="toggle-menu__text">Menu</span>
		</a>

		<nav class="header-menu" id="js-header-menu" role="navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'tobiyama' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?> -->
			<div class="g-nav">
				<a href="/news.html" class="g-nav__item">News</a>
				<a href="/works.html" class="g-nav__item">Works</a>
				<a href="/services.html" class="g-nav__item">Services</a>
				<a href="/company.html" class="g-nav__item">Company</a>
				<a href="/contact.html" class="g-nav__item">Contact</a>
			</div>
			<div class="sub-nav">
				<a href="#" class="sub-nav__item">MAKI Dance Company</a>
				<a href="#" class="sub-nav__item">未完成自主映画祭</a>
				<a href="#" class="sub-nav__item is-icon is-twitter">
					<img src="/images/icon_twitter_brown_pc.png" class="js-responsive-image" alt="twitter">
				</a>
				<a href="#" class="sub-nav__item is-icon is-facebook">
					<img src="/images/icon_facebook_brown_pc.png" class="js-responsive-image" alt="facebook">
				</a>
			</div>
		</nav>
	</div>
</header>
<div class="l-wrapper">
	<main class="l-main" id="main">
