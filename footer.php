<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tobiyama
 */

?>

	</main>

	<footer class="l-footer" role="contentinfo">
		<div class="l-footer__inner footer">
			<div class="footer__col footer-profile">
				<div class="footer-profile__title">株式会社飛山企画</div>
				<div class="footer-profile__address">東京都世田谷区大原1-5-22<br>SO1522-104</div>
				<div class="footer-profile__social footer-social">
					<a href="#" class="footer-social__item is-twitter">
						<img src="/images/icon_twitter_white_pc.png" class="js-responsive-image" alt="Twitter">
					</a>
					<a href="#" class="footer-social__item is-facebook">
						<img src="/images/icon_facebook_white_pc.png" class="js-responsive-image" alt="Facebook">
					</a>
				</div>
			</div>
			<p class="footer__col footer-copyright"><small>&copy; 2016 飛山企画</small></p>
		</div>
	</footer>
</div>

<!-- Facebook SDK -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<% } %>
<script src="/js/main.js"></script>
<%# <script src="/js/svg4everybody.min.js"></script>
<script>svg4everybody();</script>%>

<?php wp_footer(); ?>

</body>
</html>
