<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tobiyama
 */

get_header(); ?>

		<section class="l-section kv">
			<img src="/images/kv_pc.jpg" class="js-responsive-image" alt="映像便利屋 株式会社飛山企画">
		</section>

		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		<section class="l-section top-news news-list">
      <div class="l-section__inner top-news__inner">
        <div class="top-news__main">
          <div class="article-list">
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_01.jpg" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_02.jpg" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_works.png" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_event.png" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_news.png" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
            <a href="/news-detail.html" class="article-list__item article-list-item">
              <span class="article-list-item__image">
                <img src="/images/thumb_01.jpg" alt="">
              </span>
              <span class="article-list-item__text">
                <span class="info">
                  <span class="date">2016.11.1</span>
                  <span class="tag">制作実績</span>
                </span>
                <span class="title">SBSテレビ『超ドSナイトの夜』</span>
                <span class="body">SBSテレビの連続ドラマ『超ドSナイトの夜』（柴田啓佑監督）のオープニング映像、予告編を担当しました。本日23:55～第0話放送。実在するバラエティ番組の裏側が舞台の面白いドラマです。毎週火曜、静岡にお住…</span>
              </span>
            </a>
          </div>
          <a href="#" class="top-news__more button">MORE</a>
        </div>
        <div class="top-news__aside banner-area">
          <a href="#" class="banner-area__item">
            <img src="/images/banner_mikansei.jpg" alt="未完成自主映画祭">
          </a>
          <%# <a href="#" class="banner-area__item">
            <img src="/images/link_events.png" alt="">
          </a>%>
          <a href="#" class="banner-area__item">
            <img src="/images/banner_dance.jpg" alt="MAKI DANCE COMPANY">
          </a>
        </div>
      </div>
    </section>
    <section class="l-section top-services">
      <div class="l-section__inner top-services__inner">
        <h1 class="top-services__title">
          <img src="/images/h_services_pc.png" alt="" class="js-responsive-image">
        </h1>
        <ul class="top-services__list service-list">
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
          <li class="service-list__item">
            <img src="/images/icon_service.png" alt="" class="icon">
            <p class="name">映画制作</p>
          </li>
        </ul>
        <a href="#" class="top-services__more is-white button">MORE</a>
      </div>
    </section>

<?php
get_sidebar();
get_footer();
